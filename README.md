# Writing applications in Angular 2 [part 18] #

## SockJS SpringFramework backend with Angular2 SockJS client ##

In the [part 16](https://bitbucket.org/tomask79/angular2-websockets-spring) I created [WebSocket](http://docs.spring.io/spring/docs/current/spring-framework-reference/html/websocket.html) application with Spring backend and Angular 2 client. Anyway, writing strictly raw WebSocket applications today is still risky, because not every browser and backend web container supports them. Check following link for more details:

```
https://github.com/Atmosphere/atmosphere/wiki/Supported-WebServers-and-Browsers
```

One of very popular solutions today how to overcome this problem is to use [SockJS protocol](http://sockjs.github.io/sockjs-protocol/sockjs-protocol-0.3.3.html). SockJS offers fallback to [Http Long Polling](https://www.pubnub.com/blog/2014-12-01-http-long-polling/) or [Http Streaming](https://cs.wikipedia.org/wiki/HTTP_Live_Streaming)...So let's build Spring backend first.

**Spring configurer for SockJS:**
```
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
public class SockJSConfigurer implements WebSocketConfigurer {
	
	/**
	 * Register {@link WebSocketHandler}s including SockJS fallback options if desired.
	 */
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		registry.addHandler(myHandler(), "/sockjsTest").withSockJS();
	}

    @Bean
    public WebSocketHandler myHandler() {
        return new SockJSHandler();
    }
}
```
It is absolutely the same as for WebSockets, just handler registration is needed with call of **withSockJS** method. For more information definitely check again perfect documentation from [Spring WebSockets](http://docs.spring.io/spring/docs/current/spring-framework-reference/html/websocket.html). 

In the test demo application we're going to be pushing server time to client every second. As you can see SockJSHandler is again identical when using raw webSockets:

```
package org.sockjs.test;

import java.io.IOException;
import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

public class SockJSHandler extends TextWebSocketHandler {
	
	private WebSocketSession session;
	
    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) 
    		throws IOException {
    	System.out.println("handleMessageText invoked: "+message.getPayload());
    	session.sendMessage(new TextMessage(""+new Date()));
    }
    
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		this.setSession(session);
	}
    
    @Scheduled(fixedDelay=1000)
    public void pushFromServer() throws IOException {
    	if (getSession() != null && getSession().isOpen()) {
    		getSession().sendMessage(new TextMessage(""+new Date()));
    	}
    }

	public WebSocketSession getSession() {
		return session;
	}

	public void setSession(WebSocketSession session) {
		this.session = session;
	}
}

```
Btw to glue Angular 2 SockJS client and Spring Framework SockJS backend, we're going to use [Angular-CLI](https://cli.angular.io/). Definitely check this out. Even if it's beta it guides you in creation of your application in the best practices recommended by the Angular 2 core team. Anyway, to use result build produced by Angular-CLI we need to expose **/dist** directory.

```
@Configuration
@EnableWebMvc
@EnableScheduling
@ComponentScan(value = "org.sockjs.test")
public class SpringMvcConfigurer extends WebMvcConfigurerAdapter {
	 @Override
	 public void addResourceHandlers(final ResourceHandlerRegistry registry) {
	     registry.addResourceHandler("/**").addResourceLocations("/dist/");
	     registry.addResourceHandler("/vendor**").addResourceLocations("/dist/vendor/");   
	     registry.addResourceHandler("/app**").addResourceLocations("/dist/app/");   
	 }

	 @Override
	 public void addViewControllers(final ViewControllerRegistry registry) {
	     registry.addViewController("/").setViewName("forward:/index.html");
	 }
}
```

## SockJS Angular 2 client ##

Angular2 part will contain component which will be getting server time from Angular2 service powered by javascript third party library [SockJS client](https://cdnjs.com/libraries/sockjs-client).

First, Observable service:


```
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import 'sockjs-client';

declare var SockJS:any;

@Injectable()
export class SockJsService {
    private sockJsSubject: Subject<string> = new Subject<string>();

    public connect(_url: string): void {
         var sock = new SockJS(_url);
         var self = this;
         sock.onopen = function() {
            sock.send('Initializing SockJS connection from Angular2....');
         };

         sock.onmessage = function(e: any) {
            self.sockJsSubject.next(e.data);
         };

         sock.onclose = function() {
         };
    }

    public onSockJsMessage(): Observable<string> {
        return this.sockJsSubject.asObservable();
    }
}
```
Btw last time I made a horrible mistake. **Never expose from your Observable service Subject**! Because you're giving to your subscribers tool for publishing data and you definitely don't want this..:-). So notice the row with calling of **asObservable() method**. Also notice the following two rows:


```
import 'sockjs-client';

declare var SockJS:any;
```

This is the way of importing third party libraries to Angular 2 typescript components. Second row is an entry point to your third party library. **I will be talking about it more soon in the other posts**.

## Glue everything together with Angular-CLI ##

To build your Angular 2 project with Angular-CLI you have to use:

```
ng build
```
so let's add this command into the build process defined in the **Maven Build**:

```
<build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-war-plugin</artifactId>
        <configuration>
          <failOnMissingWebXml>false</failOnMissingWebXml>
          <warSourceExcludes>**/node_modules/**</warSourceExcludes>
        </configuration>
      </plugin>
      
      <plugin>
  		<groupId>org.apache.tomcat.maven</groupId>
  		<artifactId>tomcat7-maven-plugin</artifactId>
  		<version>2.2</version>
  		<configuration>
    		<path>/</path>
    		<contextReloadable>true</contextReloadable>
  		</configuration>
	  </plugin>
	  
	  <plugin>
			<groupId>org.codehaus.mojo</groupId>
			<artifactId>exec-maven-plugin</artifactId>
			<version>1.3.2</version>
			<executions>
				<execution>
					<id>npm i</id>
					<goals>
						<goal>exec</goal>
					</goals>
					<phase>initialize</phase>
					<configuration>
						<executable>npm</executable>
						<arguments>
							<argument>i</argument>
						</arguments>
						<workingDirectory>${basedir}/src/main/webapp</workingDirectory>
					</configuration>
				</execution>
				
				
				<execution>
					<id>npm_clean</id>
					<goals>
						<goal>exec</goal>
					</goals>
					<phase>clean</phase>
					<configuration>
						<executable>npm</executable>
						<arguments>
							<argument>run</argument>
							<argument>clean</argument>
						</arguments>
						<workingDirectory>${basedir}/src/main/webapp</workingDirectory>
					</configuration>
				</execution>
				
					
				<execution>
					<id>npm run build</id>
					<goals>
						<goal>exec</goal>
					</goals>
					<phase>compile</phase>
					<configuration>
						<executable>npm</executable>
						<arguments>
							<argument>run</argument>
							<argument>build</argument>
						</arguments>
						<workingDirectory>${basedir}/src/main/webapp</workingDirectory>
					</configuration>
				</execution>
			</executions>
		</plugin>	 
    </plugins>
  </build>
```
Also notice how we excluded node_modules directory from packing it into war.

## Testing the demo ##

* mvn clean install (in the root directory with pom.xml)
* mvn tomcat7:run-war-only
* hit the browser at http://localhost:8080

In the page you should see server time being updated every second.

Enjoy the SockJS protocol!

regards

T.