package org.sockjs.test;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
public class SpringMVCController {
	
	@RequestMapping(path = "/greeting/{personId}", method=RequestMethod.GET)
	public String greetings(@PathVariable("personId") String personId) {
		return "Hello from Spring 4: "+personId;
	}
}
