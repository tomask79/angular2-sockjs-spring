package org.sockjs.test;

import java.io.IOException;
import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

public class SockJSHandler extends TextWebSocketHandler {
	
	private WebSocketSession session;
	
    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) 
    		throws IOException {
    	System.out.println("handleMessageText invoked: "+message.getPayload());
    	session.sendMessage(new TextMessage(""+new Date()));
    }
    
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		this.setSession(session);
	}
    
    @Scheduled(fixedDelay=1000)
    public void pushFromServer() throws IOException {
    	if (getSession() != null && getSession().isOpen()) {
    		getSession().sendMessage(new TextMessage(""+new Date()));
    	}
    }

	public WebSocketSession getSession() {
		return session;
	}

	public void setSession(WebSocketSession session) {
		this.session = session;
	}
}
