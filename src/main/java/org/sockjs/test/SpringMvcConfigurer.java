package org.sockjs.test;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
@EnableScheduling
@ComponentScan(value = "org.sockjs.test")
public class SpringMvcConfigurer extends WebMvcConfigurerAdapter {
	 @Override
	 public void addResourceHandlers(final ResourceHandlerRegistry registry) {
	     registry.addResourceHandler("/**").addResourceLocations("/dist/");
	     registry.addResourceHandler("/vendor**").addResourceLocations("/dist/vendor/");   
	     registry.addResourceHandler("/app**").addResourceLocations("/dist/app/");   
	 }

	 @Override
	 public void addViewControllers(final ViewControllerRegistry registry) {
	     registry.addViewController("/").setViewName("forward:/index.html");
	 }
}
