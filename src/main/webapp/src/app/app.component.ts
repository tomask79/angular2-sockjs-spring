import { Component, OnInit } from '@angular/core';
import { SockJsService } from './sockjs.service';

@Component({
    selector: 'app-root',
    template: `
        <h1>SockJS with Angular2 and Spring</h1>
        <hr>
        <span>Server time updated by SockJS: {{serverTime}}</span>
        `,
    providers: [SockJsService]
})
export class AppComponent implements OnInit { 
    private serverTime: string;

    constructor(public _sockjsService: SockJsService) {
    }

    ngOnInit(): void{
        this._sockjsService.connect('http://localhost:8080/sockjsTest');
        this._sockjsService.onSockJsMessage().subscribe(data => {
            this.serverTime = data;
        });
    }
}