import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import 'sockjs-client';

declare var SockJS:any;

@Injectable()
export class SockJsService {
    private sockJsSubject: Subject<string> = new Subject<string>();

    public connect(_url: string): void {
         var sock = new SockJS(_url);
         var self = this;
         sock.onopen = function() {
            sock.send('Initializing SockJS connection from Angular2....');
         };

         sock.onmessage = function(e: any) {
            self.sockJsSubject.next(e.data);
         };

         sock.onclose = function() {
         };
    }

    public onSockJsMessage(): Observable<string> {
        return this.sockJsSubject.asObservable();
    }
}